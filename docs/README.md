# Documentation
[[_TOC_]]

## Introduction
The AAF spec has been widely regarded as a convoluted standard, with an exceedingly complex ~150MB 780,000 line of code SDK and a somewhat cryptic documentation. Furthermore, interfacing it can be even more troublesome as various software capable of exporting AAF files use their own different implementations, such that there is no clear standard but rather a number of mildly different species of AAF, that can hopefully be used to transfer project data between DAW and filmmaking software. Despite the format's popularity, even third party information on AAF development is scarce. Instead of attempting to study the official documentation and implementing it rigorously, we used the "poke it with a stick" approach of using pyaaf2 to look around a number of AAF files, dump their content and figure out how to extract relevant data for a minimal post-production workflow. The scope of this documentation is simply to help maintain the underlying script as well as shed some light on the practical usage of pyaaf2.

## Overview
All project data is structured inside the **content** module. Here you can find a list of **mobs**. Mobs are generic storage classes that come in different flavours:
* **CompositionMob** holds all the data regarding the project timeline. An AAF file may contain one or more composition mobs that can be accessed iterating over `aaf.content.toplevel()`, where `aaf` is an object returned by `aaf2.open()`.
* **MasterMob** contains metadata about a clip. A master mob can contain references to one (for mono clips) ore several (for multichannel clips) source mobs.
* **SourceMob** contains metadata about a source file.
* The actual data is called **Essence** and is referenced by the source mob. Essence can be embedded in the AAF, or given as a file URL.

Metadata about the AAF file can be found inside the `aaf.header`.

## Structure of a project
### The composition mob
Accessing the `slots` property of a composition mob will give you a list of **TimelineMobSlot** or **EventMobSlot** objects. You can check the `media_kind` property of a slot to see what type of slot it is:
* "Sound" means the slot is the equivalent of an audio track. Note that AAF splits stereo or multichannel tracks into separate mono tracks.
* "Picture" denotes a video track.
* "Timecode" or "LegacyTimecode", unknown
* "DescriptiveMetadata" is used to store data such as project markers.

Each slot has a property called `edit_rate`, which is the equivalent of "frames per second". The value of `edit_rate` is an **AAFRational** object, which is the AAF equivalent of a floating point number. Simply divide its `numerator` by its `denominator` to get the actual value. This edit rate will be used throughout the slot to indicate timings.

Other useful properties of the CompositionMob:
* `composition_mob.name`, the name of the project
* `composition_mob["CreationTime"]`
* `composition_mob["LastModified"]`

### Sound slots
Each sound slot has a `segment` property, where the actual data is stored. The segment is typically a **Sequence** class, but can sometimes be of type **OperationGroup**, which will in turn contain a `segments` list with a single element of type Sequence. Any segment has a `length` property which returns the segment's length. All timing data in AAF is expressed in frames; to calculate the actual time in seconds, you need to divide the amount of frames by the edit rate of the slot. *Note that occasionally the OperationGroup's length might not be correctly stored; if timings are off, use the length of the data segment (*`segment.segments[0]`*) instead.*
The reasoning for OperationGroup segments is that they also add an `operation` to the track (such as panning data), as well as a list of `parameters` associated with the operation. So far we have encountered the following operations:

| Operation name | Parameter names | Parameter types | Description |
|----------------|------------|-----------------|-------------|
| Mono Audio Pan | Pan value | ConstantValue (AAFRational) | The track's panning as an AAFRational containing a floating point from 0 to 1 |
| Audio Pan | Pan | VaryingValue | The track's panning automation as a series of points (values as above) |

To decode a **ConstantValue**, you have to access its `value` property, which can store an arbitrary type. **VaryingValue** parameters are decoded as follows:

```python
for point in varying_value["PointList"]:
    time = point.time / segment_duration
    # Time is a float (not AAFRational) between 0.0 and 1.0
    # marking the position of the point inside the track or item
    value = point.value
    # Also a float
```

A sequence holds a number of `components` to mark items, fades, and silence. A component can be one of the following classes:
* **SourceClip**, an audio item
* **OperationGroup**, an audio item with associated operations
* **Filler**, silence
* **Transition**, a fade or crossfade

To recreate the track's timeline you will need to look at each component's `length`, expressed as frames. Note that while adding up the length of SourceClip, OperationGroup and Filler components will give you the total amount of time from the beginning of the track, the length of Transition components needs to be subtracted, as it causes the two blended components to overlap.

![Sequence](images/sequence.png "Sequence")

**SourceClip** components have a `mob` and a `slot_id` property which indicates its associated **MasterMob** and slot index. See [extracting essence](#extracting-essence) below for details. To retrieve the offset of the audio source in the particular item use the `start` property, which is also expressed as frames.

**OperationGroup** objects represent more complex items. As in the case of tracks, An OperationGroup contains an `operation` whose `name` denotes the kind of operation the item is being subjected to, as well as a list of `parameters` associated with the operation. `segments` points to the data that the operation is being applied to. Inside `segments` you might find SourceClip objects, but there can also be another nested OperationGroup, in case multiple operations are stacked. Here is a list of operations that we observed:

| Operation name | Parameter names | Parameter types | Description |
|----------------|-----------------|-----------------|-------------|
| Mono Audio Gain | Amplitude multiplier | Either ConstantValue (AAFRational) or VaryingValue | Item volume if ConstantValue or Per-item volume automation if VaryingValue (note that point times are calculated relative to the *item* such that 0.0 is the item's start and 1.0 is the item's end). 0.0 is mute, 0.5 is -6dB, 1.0 is +0dB, 2.0 is +6dB. Observed in AAFs generated by Premiere. |
| Audio Gain | Amplitude | Same as above | Same as above |
| | Avid Control Clip Ratio | VaryingValue | Also possibly referring to volume automation. Seen in X2Pro AAFs.
| | Pan Vol - Is Trim Gain Effect | ConstantValue (int) | Unknown meaning |
| Audio Channel Combiner | - | - | Seen in AAFs that do not contain embedded essence and indicates the referenced clip is a stereo or multichannel file. An OperationGroup with this operation contains two SourceClip segments pointing to the same file.
| (unnamed) | (unnamed) | VaryingValue | Unknown meaning, point lists contain no elements. Seen in ProTools AAFs.

**Transition** components represent fades. A transition that occurs between a Filler and a SourceClip (or OperationGroup) is a fade in. The reverse is a fade out, and a transition between two SourceClip / OperationGroup components represents a crossfade. To retrieve the type of interpolation used, call
```python
transition["OperationGroup"].value.parameters.value[0].interpolation.name
```
which will return either "LinearInterp" or "PowerInterp".

### Picture slots
The primary difference between sound and picture slots is that a composition can only contain one picture slot. Accessing the `segment` property yields a **NestedSegment** object, which contains a list of **Sequence** items in its `slots`, representing the video tracks. Sequences are very similar to the ones found in sound slots: They contain a number of `components` which can be one of **SourceClip**, **OperationGroup**, **Filler** or **Transition**. SourceClip, Transition and Filler are mostly identical to the ones met in sound slots. An OperationGroup may include a number of common video operations:

| Operation name | Parameters |
|----------------|------------|
| Video Position | Position OffsetX |
| | Position OffsetY |
| Video Scale | ScaleX |
| | ScaleY |
| Opacity | Opacity |
| Video Speed Control | Speed Ratio |

Occasionally, the encoder creates an operation named "Unknown effect", without any parameters.

### Extracting markers
An EventMobSlot whose `media_kind` is "DescriptiveMetadata" may contain components of class **DescriptiveMarker**. These contain data regarding project markers: `component["Position"]` indicates the marker's position in time (divide by the EventMobSlot's `edit_rate` to get the value in seconds), `component["Comment"]` is the marker's name, and `component["CommentMarkerColour"]` gives you the marker's colour as a dict with "red", "green" and "blue" keys, whose values are 16-bit per pixel integers.

## Extracting essence
To simply extract all audio data within an AAF file, one can simply access `aaf.content.essencedata` which is a list of **EssenceData** objects. Reading raw essence data is done as follows:
```python
stream = essence_data.open()
data = stream.read()
stream.close()
# You can use the data for example by dumping it into a file:
with open("my_essence.wav", "wb") as f:
    f.write(data)
    f.close()
```
However, the EssenceData object itself provides no information about the file, such as its original filename or encoding. This data is stored in a **SourceMob**. Additional data about the role of sources within the project is contained in a **MasterMob**. 

```mermaid
flowchart TD
  subgraph MM2[Master Mob 2]
    S2[slot1.segment]
    S3[slot2.segment]
  end
  subgraph MM1[Master Mob 1]
    S1[slot.segment]
  end
  S1 --> SM1([Source mob]) --> E1{{Essence}}
  S2 --> SM2([Left channel\nsource mob]) --> E2{{Essence}}
  S3 --> SM3([Right channel\nsource mob]) --> E3{{Essence}}
```
`aaf.content.mastermobs()` returns a generator containing all the master mobs. The mobs' `name` indicates the name of the associated clip. Each mob has one or multiple `slots`, typically one per channel. Each slot has a separate `name` and a `slot_id` (an integer that is used by items to uniquely identify the slot they belong to), as well as a `segment`, which in turn contains a `mob` property linking to a **SourceMob** that can be used to access the essence and its metadata.

If `source_mob.essence` is null, the essence is not embedded within the AAF file, but rather linked to externally. Use `source_mob.descriptor.locator.pop()["URLString"].value` to retrieve its filename. Otherwise, `source_mob.essence` will point to an **EssenceData** object that can be extracted as previously mentioned. The value follows a generic URL scheme, since AAF technically supports network-based sources.
`source_mob.descriptor.keys()` should normally (but does not always!) contain a key called "ContainerFormat" (access it with `source_mob.descriptor["ContainerFormat"].value.name`) which describes the format in which data is encoded. This is important because even though most of the time simply dumping the essence data into a wave file will suffice, a container format whose value is "MXF" appears to be associated with essence data in a raw PCM format, so dumping the data into a file will not work as headers have been stripped. If this is the case, you need to recreate the file by accessing `source_mob.descriptor["QuantizationBits"].value`, which gives you the essence's sample depth, and `source_mob.descriptor["SampleRate"].value` for sampling rate, to help interpret the raw data in the essence object.

## AAF Metadata
`aaf.header["IdentificationList"][0]` should contain an Identification object. Calling its `keys()` method gives you a list of available fields about the software that was used to encode the file, such as
* `ProductName`, for example "ProTools" or "Premiere Pro"
* `CompanyName`, self explanatory
* `ProductVersionString` and `ProductVersion`, the difference being ProductVersion gives you an object with values for "major", "minor", "tertiary", "patchLevel" and "type", while ProductVersionString simply gives you a human readable string, eg. 15.4.1
* `Platform`, the platform where the AAF was generated. Example values: "AAFSDK (Win64)", "AAFSDK (MacOS X)"
* `Date`, the date when the file was encoded

